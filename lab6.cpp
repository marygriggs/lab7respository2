/**********************************************
				Mary Griggs - mgriggs	
				1021 lab section 2
				LAB 6 - intro to c++
***********************************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

	using namespace std;

	typedef struct Employee{
		string lastName;
		string firstName;
		int birthYear;
		double hourlyWage;
	}employee;

	bool name_order(const employee& lhs, const employee& rhs);

	int myrandom (int i) { return rand()%i;}

int main() {
	int i;
	// IMPLEMENT as instructed below

	/*This is to seed the random generator */
	srand(unsigned (time(0)));


	/*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee employees[10];

	for(i = 0; i<10; i++){

		cout << "What is employee " << i+1 << "'s first name?" << endl; 
		cin >> employees[i].firstName; 

		cout << "What is employee " << i+1 << "'s last name?" << endl; 
		cin >> employees[i].lastName; 

		cout << "What is employee " << i+1 << "'s birth year?" << endl; 
		cin >> employees[i].birthYear; 
		
		cout << "What is employee " << i+1 << "'s hourly wage?" << endl; 
		cin >> employees[i].hourlyWage; 
	}
	/*After the array is created and initialzed we call random_shuffle() see the
		*notes to determine the parameters to pass in.*/
	
	random_shuffle(employees, employees+10, myrandom);

	/*Build a smaller array of 5 employees from the first five cards of the array created
		*above*/
	
	employee smallArr[5] = {employees[0],employees[1],employees[2],employees[3],employees[4]};

	/*Sort the new array.  Links to how to call this function is in the specs
		*provided*/
	
	sort(smallArr, smallArr+5, name_order);

	/*Now print the array below */

	for(auto x: smallArr){

		cout << setw(10) <<  right << x.lastName << ", " << x.firstName << endl;
		cout << setw(10) << right << x.birthYear << endl;
		cout << setprecision(2) << showpoint << fixed << setw(10) <<right << x.hourlyWage << endl;
	}
	return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
	// IMPLEMENT
	
	if(lhs.lastName < rhs.lastName){
		return(1);
	}else{
		return(0);
	}
}
